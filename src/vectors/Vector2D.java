package vectors;

import java.io.Serializable;

/**
 * Created by Ricardo on 20/10/2016.
 */
public class Vector2D implements Vector<Vector2D>, Serializable{

    public class R2 implements Serializable{
    }

    public double x;
    public double y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void add(Vector2D b){
        x += b.x;
        y -= b.y;
    }

    public void sub(Vector2D b){
        x -= b.x;
        y -= b.y;
    }

    public void multiply(double d){
        x *= d;
        y *= d;
    }

    public Vector2D plus(Vector2D b){
        return new Vector2D(x+ b.x, y+b.y);
    }

    public Vector2D minus(Vector2D b){
        return new Vector2D(x-b.x, y-b.y);
    }

    public Vector2D times(double b){
        return new Vector2D(x*b, y*b);
    }

    public double length(){
        return Math.sqrt(x*x+y*y);
    }



    public Vector2D unit(){
        return new Vector2D(x,y).times(1/this.length());
    }

    public Vector2D unitSub(Vector2D b){
        return this.minus(b).unit();
    }

    public Vector2D hadamardProduct(Vector2D b){
        return new Vector2D(x*b.x, y*b.y);
    }

    @Override
    public String toString() {
        return "x: "+ x+" y:"+y;
    }
}
