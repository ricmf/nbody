package vectors;

import java.util.function.Function;

/**
 * Created by Ricardo on 01/12/2016.
 */
public interface TransformacaoLinear<T extends Vector,U extends Vector> extends Function<T,U> {
}
