package vectors;

/**
 * Created by Ricardo on 20/10/2016.
 */
public interface Vector<U extends Vector<U>> {
    public U plus(U b);
    public default U minus(U b){
        return this.plus(b.times(-1));
    };
    public U times(double b);
    public double length();
    public default U unit(){
        return this.times(1/this.length());
    }

    public void add(U b);

    public void sub(U b);

    public void multiply(double d);


}
