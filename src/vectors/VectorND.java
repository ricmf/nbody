package vectors;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by Ricardo on 20/10/2016.
 */
public class VectorND implements Vector<VectorND>, Serializable {


    protected double [] coordinates;

    public VectorND(int dimention){
        coordinates = new double[dimention];
    }

    public VectorND(double ... coordinates) {
        this.coordinates = coordinates;
    }

    public void add(VectorND b){
        for (int i =0; i< coordinates.length; ++i){
            coordinates[i] += b.coordinates[i];
        }
    }

    public void sub(VectorND b){
        for (int i =0; i< coordinates.length; ++i){
            coordinates[i] -= b.coordinates[i];
        }
    }

    public void multiply(double d){
        for (int i =0; i< coordinates.length; ++i){
           coordinates[i] *= d;
        }
    }

    public VectorND plus(VectorND b){
        double[] coordinates = Arrays.copyOf(this.coordinates, this.coordinates.length);
        for (int i =0; i< coordinates.length; ++i){
            coordinates[i] += b.coordinates[i];
        }
        return  new VectorND(coordinates);
    }

    public VectorND times(double d){
        double[] coordinates = Arrays.copyOf(this.coordinates, this.coordinates.length);
        for (int i =0; i< coordinates.length; ++i){
            coordinates[i] *= d;
        }
        return new VectorND(coordinates);
    }

    public double length(){
        double d = 0;
        for (double b :coordinates){
            d+= b*b;
        }
        return Math.sqrt(d);
    }

    public double elementAt(int n){
        return coordinates[n];
    }

    public void set(int n, double v){
        coordinates[n] = v;
    }


    public int dimention(){
        return coordinates.length;
    }

}
