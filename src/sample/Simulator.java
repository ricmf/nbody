package sample;

import vectors.Vector;
import vectors.Vector2D;
import vectors.VectorND;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by Ricardo on 20/10/2016.
 */
public class Simulator<T extends Vector<T> & Serializable> implements Serializable {

    private final List<Body<T>> bodies;
    private final double stepSize;

    private String title;

    public static double g = 1e-1;


    public void setTitle(String title) {
        this.title = title;
    }

    public static Simulator<Vector2D> random2DSimulation(int bodyCount, double stepSize){
        double [] masses = new double[bodyCount];
        double [] speedxs = new double[bodyCount];
        double [] speedys = new double[bodyCount];
        double totalMass = 0;
        double totalMomentumX = 0;
        double totalMomentumY = 0;
        Random r = new Random();
        double g = r.nextGaussian();
        masses[0] = 1+ Math.random()*10000;
        totalMass += masses[0];
        for (int i=1; i<bodyCount; ++i){
            g = r.nextGaussian();
            masses[i] = 1+ ((g-1/2)*(g-1/2)*(g-1/2)*(g-1/2))*50;
            speedxs[i] = Math.random()*20;
            speedys[i] = Math.random()*20;
            totalMass += masses[i];
            totalMomentumX += speedxs[i]*masses[i];
            totalMomentumY += speedys[i]*masses[i];
        }
        for (int i=1; i<bodyCount; ++i){
            speedxs[i] -= totalMomentumX/totalMass;
            speedys[i] -= totalMomentumY/totalMass;
        }
        Body [] bodies = new Body[bodyCount];
        for (int i = 0; i< bodyCount ; ++i){
            Body<Vector2D> b = new Body<>(masses[i],
                    new Vector2D(20+(Math.random())*20,20+(Math.random())*20),
                    new Vector2D(speedxs[i],speedys[i]));
            bodies[i] = b;
        }
        return new Simulator<Vector2D>(bodies, stepSize);
    }

    public static Simulator<VectorND> randomNDSimulation(int bodyCount, double stepSize, int dimention){
        double [] masses = new double[bodyCount];
        double [][] speeds = new double[bodyCount][dimention];
        double [][] positions = new double[bodyCount][dimention];
        double totalMass = 0;
        double [] totalMomentum = new double[dimention];
        Random r = new Random();
        for (int i = 1; i < bodyCount; ++i) {
            for (int j = 0; j<dimention; ++j) {
                positions[i][j] = (Math.random()-0.5)*20;
            }
            g = r.nextGaussian();
            masses[i] = 1 + g *g* 500*dimention;
            totalMass += masses[i];
            for (int j = 0; j<dimention; ++j) {
                int n = (j-1) % dimention;
                if (n < 0)                {
                    n += dimention;
                }
                speeds[i][j] = (Math.random()-0.5)*20*Math.signum(positions[i][n]);
                totalMomentum[j] += speeds[i][j] * masses[i];
            }
        }
        for (int j = 0; j<dimention; ++j) {
            for (int i = 1; i < bodyCount; ++i) {
                speeds[i][j] -= totalMomentum[j] / totalMass;
            }
        }
        Body [] bodies = new Body[bodyCount];
        for (int i = 0; i< bodyCount ; ++i){
            bodies[i] = new Body<VectorND>(masses[i],
                    new VectorND(positions[i]),
                    new VectorND(speeds[i]));
        }
        return new Simulator<VectorND>(bodies, stepSize);
    }

    private Simulator(Body<T>[] bodies, double stepSize) {
        this.bodies = new ArrayList<>();
        this.bodies.addAll(Arrays.asList(bodies));
        this.stepSize = stepSize;
    }

    public List<Body<T>> getBodies() {
        return bodies;
    }

    private static double epsolon = 2e-2;



    public void step(){
        //long l = System.nanoTime();
        double gtimestep = g*stepSize;
        for (int i= 0 ; i<  bodies.size(); ++i){
            Body<T> b = bodies.get(i);
            T halfPosition = b.position.plus(b.speed.times(stepSize/2));
            T ithSpeed = b.speed.times(1);
            for (int j= i+1 ; j<  bodies.size(); ++j){
                Body<T> b2 = bodies.get(j);
                    T firstDistanceVector = b2.position.minus(b.position);
                    T distanceVector = b2.position.minus(halfPosition);
                    double firstDistanceVectorMagnitude = distanceVector.length()+epsolon;
                    double distanceVectorMagnitude = distanceVector.length()+epsolon;
                    double firstForceMagnitude = gtimestep/distanceVectorMagnitude/distanceVectorMagnitude/distanceVectorMagnitude;
                    double forceMagnitude = gtimestep/distanceVectorMagnitude/distanceVectorMagnitude/distanceVectorMagnitude;
                    b.speed.add(distanceVector.times(b2.mass*forceMagnitude).plus(firstDistanceVector.times(b2.mass*firstForceMagnitude/2)));
                    b2.speed.add(distanceVector.times(-b.mass*forceMagnitude).plus(firstDistanceVector.times(-b.mass*firstForceMagnitude/2)));
                    //b2.speed.add(distanceVector.times(b.mass*-forceMagnitude));
            }
            b.position.add(b.speed.plus(ithSpeed).times(stepSize/2));
        }
        //System.out.println(System.nanoTime()-l);
    }

    /*public void step(){
        //long l = System.nanoTime();
        double gtimestep = g*stepSize;
        for (int i= 0 ; i<  bodies.size(); ++i){
            Body b = bodies.get(i);
            for (int j= i+1 ; j<  bodies.size(); ++j){
                Body b2 = bodies.get(j);
                Vector2D distanceVector = b2.position.minus(b.position);
                double distanceVectorMagnitude = distanceVector.length()+epsolon;
                double forceMagnitude = gtimestep/distanceVectorMagnitude/distanceVectorMagnitude/distanceVectorMagnitude;
                b2.speed.add(distanceVector.times(-b.mass*forceMagnitude));
                Vector2D bAcc = distanceVector.times(b2.mass*forceMagnitude);
                b.speed.add(bAcc);
                b.position.add(bAcc.hadamardProduct(bAcc).times(0.5));
                //b2.speed.add(distanceVector.times(b.mass*-forceMagnitude));
            }
            b.position.add(b.speed.times(stepSize));
        }
    }*/


    @Override
    public String toString() {
        return title;
    }

    public String getTitle() {
        return title;
    }
}

