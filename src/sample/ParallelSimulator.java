package sample;

import vectors.Vector2D;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Ricardo on 20/10/2016.
 */
public class ParallelSimulator {

    private final Body [] bodies;

    public static double g = 1e-1;


    public static ParallelSimulator randomSimulation(int bodyCount){
        double [] masses = new double[bodyCount];
        double [] speedxs = new double[bodyCount];
        double [] speedys = new double[bodyCount];
        double totalMass = 0;
        double totalMomentumX = 0;
        double totalMomentumY = 0;
        Random r = new Random();
        double g = r.nextGaussian();
        masses[0] = 1+ ((g-1/2)*(g-1/2)*(g-1/2)*(g-1/2))*1000;
        totalMass += masses[0];
        for (int i=1; i<bodyCount; ++i){
            g = r.nextGaussian();
            masses[i] = 1+ ((g-1/2)*(g-1/2)*(g-1/2)*(g-1/2))*10;
            speedxs[i] = Math.random()*5;
            speedys[i] = Math.random()*5;
            totalMass += masses[i];
            totalMomentumX += speedxs[i]*masses[i];
            totalMomentumY += speedys[i]*masses[i];
        }
        for (int i=1; i<bodyCount; ++i){
            speedxs[i] -= totalMomentumX/totalMass;
            speedys[i] -= totalMomentumY/totalMass;
        }
        Body [] bodies = new Body[bodyCount];
        for (int i = 0; i< bodyCount ; ++i){
            Body b = new Body(masses[i],
                    new Vector2D(20+(Math.random())*20,20+(Math.random())*20),
                    new Vector2D(speedxs[i],speedys[i]));
            bodies[i] = b;
        }
        return new ParallelSimulator(bodies);
    }

    public ParallelSimulator(Body [] bodies) {
        this.bodies = bodies;
    }

    public Body[] getBodies() {
        return bodies;
    }

    public static double epsolon = 1e-2;
/*
    public class Executor extends Thread{

        int t;
        double timestep;
        double gtimestep;

        public Executor(int t, double timestep, double gtimestep) {
            this.t = t;
            this.timestep = timestep;
            this.gtimestep = gtimestep;
        }

        @Override
        public void run() {
            for (int i = bodies.length*t/4; i < bodies.length*(t+1)/4; ++i) {
                //System.out.println(i);
                Body b = bodies[i];
                Vector2D halfPosition = b.position.plus(b.speed.times(timestep / 2));
                b.ithSpeed = b.speed.times(1);
                for (int j = i + 1; j < bodies.length; ++j) {
                    Body b2 = bodies[j];
                    Vector2D firstDistanceVector = b2.position.minus(b.position);
                    Vector2D distanceVector = b2.position.minus(halfPosition);
                    double firstDistanceVectorMagnitude = distanceVector.length() + epsolon;
                    double distanceVectorMagnitude = distanceVector.length() + epsolon;
                    double firstForceMagnitude = gtimestep / distanceVectorMagnitude / distanceVectorMagnitude / distanceVectorMagnitude;
                    double forceMagnitude = gtimestep / distanceVectorMagnitude / distanceVectorMagnitude / distanceVectorMagnitude;
                    b.speed.add(distanceVector.times(b2.mass * forceMagnitude / 2).plus(firstDistanceVector.times(b2.mass * firstForceMagnitude / 2)));
                    b2.speed.add(distanceVector.times(-b.mass * forceMagnitude / 2).plus(firstDistanceVector.times(-b.mass * firstForceMagnitude / 2)));
                    //b2.speed.add(distanceVector.times(b.mass*-forceMagnitude));
                }
            }
        }
    }


    public void step(double timestep) throws InterruptedException {
        long l = System.nanoTime();
        double gtimestep = g*timestep;
        ArrayList<Thread> threads = new ArrayList<>();
        for (int t = 0; t<4; ++t) {
            Thread td = new Executor(t, timestep, gtimestep);
            td.start();
            threads.add(td);
        }
        for (Thread t : threads){
            t.join();
        }
        for (Body b : bodies) {
            b.position.add(b.speed.plus(b.ithSpeed).times(timestep / 2));
        }
        System.out.println(System.nanoTime()-l);
    }*/



}

