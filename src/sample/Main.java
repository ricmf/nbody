package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import vectors.TransformacaoLinear;
import vectors.Vector2D;
import vectors.VectorND;

import java.io.*;
import java.text.ParseException;
import java.util.*;

public class Main extends Application {

    Simulator currentSimulation;

    boolean running = true;

    TextField numPlanetas, stepSize, nDim;

    VectorND translation = new VectorND(0,0,20);

    private Projecao2D projetor;

    public interface Projecao2D<T extends vectors.Vector> extends TransformacaoLinear<T,Vector2D>{
    }


    boolean pausado = false;


    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane h = new StackPane();
        GridPane gp = new GridPane();
        numPlanetas = new TextField("100");
        stepSize = new TextField("0.00001");
        nDim = new TextField("3");
        gp.addRow(0, new Label("Número de planetas"),numPlanetas);
        gp.addRow(1, new Label("Tamanho do passo de simulação"),stepSize);
        gp.addRow(2, new Label("Número de dimensões"),nDim);
        Button b = new Button("Iniciar simulação");
        Button verSalvas = new Button("Carregar anterior");
        Button salvar = new Button("Salvar");
        salvar.setOnAction(e->{
            if (currentSimulation.getTitle() == null) {
                Dialog<String> d = new TextInputDialog();
                d.setHeaderText("Digite um título para essa instância:");
                d.showAndWait().ifPresent(t -> save(t, false));
            }else{
                save();
            }
        });
        verSalvas.setOnAction(e->openSaves());
        GridPane.setColumnIndex(b,1);
        gp.addRow(3, verSalvas, b);
        Button pausar = new Button("Pausar/Retomar");
        gp.addRow(4, salvar, pausar);
        pausar.setOnAction(e-> pausado = !pausado);
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(20));
        b.setOnAction(a->{iniciarSimulacao();a.consume();});
        primaryStage.setTitle("Drawing Operations Test");
        Group root = new Group();
        Canvas canvas = new Canvas(1280, 640);
        HBox.setHgrow(canvas, Priority.ALWAYS);
        h.getChildren().addAll(canvas, gp);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        root.getChildren().add(h);
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> running = false);
        iniciarSimulacao();
        new Thread() {
            public void run() {
                while (running) {
                    if(!pausado) {
                        currentSimulation.step();
                    }else{
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("acabou");
            }
        }.start();
        new Thread(()->{
                while (running) {
                    try {
                        Thread.sleep(16);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Platform.runLater(()->{
                        drawing = true;
                        drawShapes(gc);
                        drawing = false;
                    });
                }
            }).start();
        scene.setOnMousePressed(e->{
            //if (e.isSecondaryButtonDown()){
            clickedY = e.getY();
            //addingPlanet = true;
            lastX = e.getX();
            lastY = e.getY();
            //currentSimulation.getBodies().add(new Body(10,new Vector2D((translationX+e.getSceneX())/canvasWidth*maximumDisplaybleX
            //        ,(translationY+e.getSceneY())/canvasHeight*maximumDisplaybleY), new Vector2D(0,0)));
            }            );
        scene.setOnMouseDragged(e->{
           if (addingPlanet){
               ((Simulator<?>)currentSimulation).getBodies().get(currentSimulation.getBodies().size()-1).setMass(Math.pow(1.05, 1+(clickedY)-e.getY()));
           }else{
               translation.set(0, translation.elementAt(0)+(lastX - e.getX())/canvasWidth*Math.abs(2+translation.elementAt(2)));
               translation.set(1, translation.elementAt(1) + (lastY - e.getY())/canvasHeight*Math.abs(2+translation.elementAt(2)));
               lastX = e.getX();
               lastY = e.getY();
           }
        });
        //translation = (VectorND) ((Body)currentSimulation.getBodies().get(0)).position;
        //scene.setOnMouseDragged(e->gc.translate(e.getX(),e.getY()));
        scene.setOnZoom(e->{
            //translation.set(0, translation.elementAt(0)- (translation.elementAt(0) - ((e.getX()-canvasWidth/2)/canvasWidth*maximumDisplaybleX))*(1-1/e.getZoomFactor()));
            //translation.set(1, translation.elementAt(1)- (translation.elementAt(1) - ((e.getY()-canvasHeight/2)/canvasHeight*maximumDisplaybleY))*(1-1/e.getZoomFactor()));
            translation.set(2, translation.elementAt(2)+(2+Math.abs(translation.elementAt(2)))*(1-e.getZoomFactor()));
        });
        scene.setOnMouseReleased(e-> addingPlanet = false);

    }



    private double lastX;
    private double lastY;

    private boolean addingPlanet = false;

    private double clickedY;

    private void iniciarSimulacao(){
        try {
            translation = new VectorND(0,0,40);
            novaSimulacao(Integer.parseInt(numPlanetas.getText()), Double.parseDouble(stepSize.getText()), Integer.parseInt(nDim.getText()));
        }catch(Exception e){
            new Alert(Alert.AlertType.ERROR, "Por favor, digite valores válidos. Use pontos ao invés de vírgulas.");
        }

    }

    private void novaSimulacao(int numPlanetas, double stepSize, int dim) {
        if (dim == 2){
            currentSimulation = Simulator.random2DSimulation(numPlanetas, stepSize);
            projetor = (Projecao2D<Vector2D>)(a)->a;
        }else{
            currentSimulation = Simulator.randomNDSimulation(numPlanetas, stepSize, dim);
            projetor = (Projecao2D<VectorND>)(a)->new Vector2D(a.elementAt(0), a.elementAt(1));
        }


        /*for (Body b : ((Simulator<?>)currentSimulation).getBodies()) {
            ArrayDeque<Vector2D> d = new ArrayDeque<>();
            trajectories.putIfAbsent(b, d);
            for (int i =0; i<100; ++i){
                d.addLast(new Vector2D(0,0));
            }
        }*/
    }

    boolean drawing = false;
    double canvasWidth = 1280;
    double canvasHeight = 640;

    double maximumDisplaybleX = 100;
    double maximumDisplaybleY = 50;


    private HashMap<Body, Deque<Vector2D>> trajectories = new HashMap<>();

    private void drawShapes(GraphicsContext gc) {
        gc.clearRect(0, 0, canvasWidth, canvasHeight);
        List<Body> l = new ArrayList<>(((Simulator<?>)currentSimulation).getBodies());
        vectors.Vector posi = ((Body)l.get(0)).position;
        if(posi instanceof VectorND){
            l.sort((b1,b2)-> ((Double) ((VectorND) b2.position).elementAt(2)).compareTo(((Double) ((VectorND) b1.position).elementAt(2))));
        }
        for (Body b : l) {
            gc.setFill(b.c);
            double radius = b.radius();
            Vector2D position = (Vector2D) projetor.apply(b.position);
            double translationX = translation.elementAt(0);
            double translationY = translation.elementAt(1);
            double translationZ = translation.elementAt(2);

            if(b.position instanceof  VectorND){
                VectorND pos = (VectorND) b.position;
                if(pos.dimention() >= 3){
                    double tamanho = radius * 30/(translationZ+ pos.elementAt(2));
                    gc.fillOval(canvasWidth/2+(position.x - translationX) /(pos.elementAt(2)+translationZ)* canvasWidth - tamanho/2,
                            canvasHeight/2+(position.y - translationY)/(pos.elementAt(2)+translationZ) * canvasHeight - tamanho
                            , tamanho, tamanho);
                }
            }else {
                gc.fillOval((position.x - translationX) / (maximumDisplaybleX) * canvasWidth - radius * 100 / maximumDisplaybleX,
                        canvasHeight * (position.y - translationY) / maximumDisplaybleY - radius * 100 / maximumDisplaybleY
                        , radius * 200 / maximumDisplaybleX + 2, radius * 100 / maximumDisplaybleY + 2);
            }
            /*for (Vector2D v : trajectories.get(b)){
                gc.fillOval((v.x - translationX)/ (maximumDisplaybleX) * canvasWidth  - radius*200/maximumDisplaybleX,
                        canvasHeight * (v.y - translationY) / maximumDisplaybleY - radius*100/maximumDisplaybleY
                        ,1,1);
            }*/
            //trajectories.get(b).addLast(position);
            //trajectories.get(b).removeFirst();
        }
    }

    private void save(String title, boolean overwrite){
        File f = new File("planetas");
        HashMap<String, Simulator> savedInstances;
        try (FileInputStream fis = new FileInputStream(f); ObjectInputStream ois = new ObjectInputStream(fis)){
            savedInstances = (HashMap<String, Simulator>)ois.readObject();
        } catch (Exception e) {
            savedInstances = new HashMap<>();
            e.printStackTrace();
        }
        if (!overwrite && savedInstances.containsKey(title)){
            throw new RuntimeException("Já existe uma instância salva com esse nome.");
        }
        currentSimulation.setTitle(title);
        savedInstances.put(title, currentSimulation);
        try (FileOutputStream fis = new FileOutputStream(f); ObjectOutputStream ois = new ObjectOutputStream(fis)){
            ois.writeObject(savedInstances);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void save(){
        save(currentSimulation.getTitle(), true);
    }

    private void openSaves(){
        File f = new File("planetas");
        HashMap<String, Simulator> savedInstances;
        try (FileInputStream fis = new FileInputStream(f); ObjectInputStream ois = new ObjectInputStream(fis)){
            new SaveScreen(((HashMap<String, Simulator>)ois.readObject()).values()).showAndWait().ifPresent(s-> {
                this.currentSimulation = s;
                if (!(((Body)s.getBodies().get(0)).position instanceof VectorND)){
                    projetor = (Projecao2D<Vector2D>)(a)->a;
                }else{
                    projetor = (Projecao2D<VectorND>)(a)->new Vector2D(a.elementAt(0), a.elementAt(1));
                }
            });
        } catch (Exception e) {
            savedInstances = new HashMap<>();
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}

