package sample;

import javafx.scene.paint.Color;
import vectors.Vector;
import vectors.Vector2D;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.SecureRandom;

/**
 * Created by Ricardo on 20/10/2016.
 */
public class Body<U extends Vector & Serializable> implements Serializable {

    double mass;
    U position;
    U speed;
    U ithSpeed;
    transient Color c;

    public Body(){
        c = new Color(random.nextDouble(),random.nextDouble(),random.nextDouble(),1);
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    private static final SecureRandom random = new SecureRandom();

    public Body(double mass, U position, U speed) {
        this();
        this.mass = mass;
        if (mass < 0){
            throw new IllegalArgumentException("mass can't be negative");
        }
        this.position = position;
        this.speed = speed;

    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double radius(){
        return  Math.log(mass)*0.4;
    }

    private void writeObject(ObjectOutputStream oos)            throws IOException {
        // default serialization
        oos.defaultWriteObject();
        // write the object
        oos.writeDouble(c.getRed());
        oos.writeDouble(c.getBlue());
        oos.writeDouble(c.getGreen());
    }

    private void readObject(ObjectInputStream ois)
            throws ClassNotFoundException, IOException {
        // default deserialization
        ois.defaultReadObject();
        c = new Color(ois.readDouble(), ois.readDouble(), ois.readDouble(), 1);
    }
}
