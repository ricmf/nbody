package sample;

import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

import java.util.Collection;
import java.util.List;

/**
 * Created by Ricardo on 22/11/2016.
 */
public class SaveScreen extends Dialog<Simulator> {


    public SaveScreen(Collection<Simulator> simulatorList){
        ListView<Simulator> l = new ListView();
        l.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        l.getItems().addAll(simulatorList);
        super.setResultConverter(bt->{
            if (bt.equals(ButtonType.OK)){
                return l.getSelectionModel().getSelectedItem();
            }else{
                return null;
            }
        });
        super.getDialogPane().getButtonTypes().addAll(ButtonType.OK,ButtonType.CANCEL);
        super.getDialogPane().setPadding(new Insets(10));
        super.getDialogPane().setContent(l);
        super.setHeaderText("Escolha uma das instâncias salvas.");
    }




}
